# Genome pattern matching

<pre>
Author  : Emmanuel Clostres  
Python  : v3.8  
Version : 16/07/2021  
</pre>

## Description
`gpm.py` is a python program that searches for the presence of one or more patterns on the forward and reverse strands of a genome in ".fasta", ".fna" or ".gz" format.

### How the program works:
1) Retrieve all fasta sequences from the <GENOME_FILE> and <PATTERN_FILE> files.
2) Converting the RNA sequences of the motif into DNA
3) For each contig sequence in the genome, the program searches for the presence of each of the motifs
4) Writing the found targets to the <OUTPUT_FILE> file


## Example
The program proposes an example with the search for 5 patterns in the genome of the bacterium *Vibrio cholerae* MS6 containing 2 chromosomes.

### Command
`python gpm.py -g Example/genome.fna.gz -p Example/pattern.fasta `

### Results
| #contig      | pattern   | strand | position        |
|--------------|-----------|--------|-----------------|
| Chromosome 1 | pattern_1 | +      | 1070            |
| Chromosome 1 | pattern_3 | -      | 3               |
| Chromosome 1 | pattern_4 | +      | 606173;2761455  |
| Chromosome 1 | pattern_4 | -      | 1941038;2566510 |
| Chromosome 2 | pattern_2 | +      | 1074024         |

No results for `pattern_5` (no display in the output file).

## Citation

Emmanuel Clostres (2021)

## License
Free and unrestricted use.