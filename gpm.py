#! /bin/python3
# -*- coding: utf-8 -*-

"""
Genome pattern matching searches all accurrences of a list of RNA/DNA pattern sequences.

# python gpm.py -g <genome.fasta.gz> -p <list_of_patterns.fasta>

"""


import getopt
import sys
import os
import time
import gzip

from datetime import datetime


def find_all(text: str, pattern: str) -> {int}:
    """
    Find all occurrences positions of pattern in text
    :param text: text where the pattern is searched
    :param pattern: pattern to be searched
    :return: set of occurrences positions (zero based) of the pattern in the text
    """
    res = set()
    start = 0
    while True:
        start = text.find(pattern, start)
        if start == -1:
            return res
        res.add(start)
        start += 1


def list_increment(list_to_increment: list, increment_value: int) -> list:
    """
    Increment each value of a list by a given value.
    :param list_to_increment: list of integers
    :param increment_value: value to increment each value in the <list_to_increment>
    """
    for i in range(len(list_to_increment)):
        list_to_increment[i] += increment_value
    return list_to_increment


def main():
    # Display parameters
    print(f"Pattern matching performed on {datetime.now().strftime('%Y-%m-%d %H:%M:%S')}")
    print(f"\nParameters\n"
          f"- Pattern file    : {PATTERN_FILE}\n"
          f"- Genome file     : {GENOME_FILE}\n"
          f"- Output file     : {OUTPUT_FILE}\n"
          f"- Force           : {FORCE_MODE}")

    # Parse genome and pattern files
    fasta_genome = parse_fasta(GENOME_FILE)
    fasta_pattern = sequence_rna_to_dna(parse_fasta(PATTERN_FILE))
    print(f"- Total contig    : {len(fasta_genome)}\n"
          f"- Total pattern   : {len(fasta_pattern)}\n")

    print(f"Processing...")
    output_file = open_file(OUTPUT_FILE, 'wt')
    output_file.write("#contig\tpattern\tstrand\tposition\n")

    # Processing on each contig in the genome file
    for contig in fasta_genome:
        contig_header, contig_seq = contig.split("\n")

        # Processing for each patten in the pattern file
        for pattern in fasta_pattern:
            pattern_header, pattern_seq = pattern.split("\n")

            # Search for all occurrences of the pattern (and its reverse complement) on the contig
            for strand, sequence in [("+", pattern_seq), ("-", reverse_complement(pattern_seq))]:
                positions_list = list(find_all(contig_seq, sequence))

                if len(positions_list) > 0:
                    # Convert 0 based positions to 1 based positions and sorts the positions
                    positions_list = list_increment(positions_list, increment_value=1)
                    positions_list.sort()

                    print(f"- Match found\n"
                          f"  -> Contig   : {contig_header[1:]}\n"
                          f"  -> pattern  : {pattern_header[1:]}\n"
                          f"  -> strand   : {strand}\n"
                          f"  -> position : {positions_list}\n")

                    # Writes the match information in the output file
                    positions_str_list = [str(pos) for pos in positions_list]
                    output_file.write(f"{contig_header[1:]}\t"
                                      f"{pattern_header[1:]}\t"
                                      f"{strand}\t"
                                      f"{';'.join(positions_str_list)}\n")

    print("Ending...")
    output_file.close()


def open_file(file: str, mode="rt"):
    """
    Opens a file according to its extension.
    :param file: File to open
    :param mode: Optional string that specifies the mode in which the file is opened
    :return: I/O object
    """
    if file.split(".")[-1] == "gz":
        return gzip.open(file, mode)
    else:
        return open(file, mode)


def parse_fasta(file_path: str) -> list:
    """
    Fetche all sequences of a fasta/fna file and store them in a list.
    :param file_path: path to the fasta file
    """
    seq_list = []
    file = open_file(file_path)
    lines = file.readlines()
    file.close()

    seq = ""
    for line in lines:
        if line[0] == ">":
            if len(seq) > 0:
                seq_list.append(seq)
            seq = line
        else:
            seq += line.rstrip().upper()
    seq_list.append(seq)
    return seq_list


def print_error(msg: str):
    """
    Write a red error message in the terminal.
    :param msg: Message to display
    """
    print(f"\033[91m[ERROR]\033[0m {msg}")


def reverse_complement(seq: str) -> str:
    """
    Returns the reverse complement of the sequence.
    :param seq: str sequence to reverse
    :return: str reverse complement of the sequence.
    """
    rev = ''
    for char in seq:
        rev = BASE_COMPLEMENT[char] + rev
    return rev


def sequence_rna_to_dna(rna_list: list) -> list:
    """
    Replaces all 'U' by 'T' in a list of "<header>\n<sequence>".
    :param rna_list: list of rna <header>\n<sequences>
    """
    dna_list = []
    for sequence in rna_list:
        header, rna = sequence.split("\n")
        dna_seq = header + "\n" + rna.replace('U', 'T')
        dna_list.append(dna_seq)
    return dna_list


def usage():
    """ Display the usage of the program in the terminal. """
    print("\nUsage :\n"
          "  python gpm.py [arguments]\n"
          "\n"
          "Arguments :\n"
          "  -f, --force                 deletes the output file if it exists (without asking the user)\n"
          "  -g, --genome                path to the fasta genome file ('.gz' file allowed) [mandatory]\n"
          "  -h, --help                  how to use the program\n"
          "  -o, --output                file to store the found matches (default is 'found_matches.tsv')\n"
          "  -p, --pattern               path to the file containing the fasta sequences of the patterns ('.gz' file "
          "allowed) [mandatory]\n"
          "\n"
          "Examples:\n"
          "  python gpm.py -g <genome.fasta> -p <list_of_patterns.fasta>\n")


# Global variables
BASE_COMPLEMENT = {'A': 'T', 'C': 'G', 'G': 'C', 'T': 'A', 'N': 'N'}
GENOME_FILE = None
PATTERN_FILE = None
OUTPUT_FILE = "found_matches.tsv"
FORCE_MODE = False


if __name__ == '__main__':
    # Fetch input arguments
    try:
        opts, _ = getopt.getopt(sys.argv[1:], 'fg:ho:p:', ['force', 'genome=', 'help', 'output=', 'pattern='])
    except getopt.GetoptError as err:
        print_error(f"{str(err)[0].upper() + str(err)[1:]}")
        usage()
        sys.exit(2)

    for option, arg in opts:
        if option == '-f' or option == '--force':
            FORCE_MODE = True
        elif option == '-g' or option == '--genome':
            GENOME_FILE = arg
        elif option == '-h' or option == '--help':
            usage()
            sys.exit(0)
        elif option == '-o' or option == '--output':
            OUTPUT_FILE = arg
        elif option == '-p' or option == '--pattern':
            PATTERN_FILE = arg

    # Check the number of arguments and the presence of mandatory arguments
    if (not GENOME_FILE) or (not PATTERN_FILE) or (len(opts) == 0):
        print_error("One or more mandatory arguments are missing")
        usage()
        sys.exit(2)
    if len(opts) > 4:
        print_error("Too many arguments.")
        usage()
        sys.exit(2)

    # Check if the input path exists
    for path in [GENOME_FILE, PATTERN_FILE]:
        if not os.path.exists(path):
            print_error(f"Path '\033[92m{path}\033[0m' not found")
            usage()
            sys.exit(2)

    # Check if the output file already exist
    path_to_remove = []
    if os.path.exists(OUTPUT_FILE):
        if FORCE_MODE:
            path_to_remove.append(OUTPUT_FILE)
        else:
            print_error(f"File '\033[92m{OUTPUT_FILE}\033[0m' already exist")
            reply = input("Delete it ? [y/n] ")
            if reply.lower() == "y" or reply.lower() == "yes":
                path_to_remove.append(OUTPUT_FILE)
            else:
                print("The program has been stopped..")
                sys.exit(2)

    # Deletes existing output files
    for path in path_to_remove:
        os.remove(path)
        print(f"The file '{path}' file has been deleted")

    start_time = time.time()
    main()
    print(f"\nExecution time    : {round(time.time() - start_time, 2)} sec")
